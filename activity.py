# Create 5 variables and output them in the command prompt in the following format:
# - "I am < name (string)> , and I am < age (integer)> years old, I work as a < occupation (string)> , and my rating for < movie (string)> is < rating (decimal)> %"

name = "Jose"
age = 38
occupation = "Writer"
movie = "One More Chance"
rating = 99.6

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}%")


# Create 3 variables, num1, num2, and num3
# - Get the product of num1 and num2
# - Check if num1 is less than num3
# - Add the value of num3 to num2

num1 = 10
num2 = 15
num3 = 20

print(num1 * num2)
print(num1 < num3)
print(num3 + num2)
